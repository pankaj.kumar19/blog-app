package models

type Blog struct {
	Id          int    `json:"id" gorm:"primaryKey"`
	Title       string `json:"title"`
	Description string `json:"description"`
	UserRefer   int    `json:"user_id"`
	User        User   `gorm:"foreignKey:UserRefer"`
}
