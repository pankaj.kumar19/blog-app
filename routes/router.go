package routes

import (
	"github.com/gin-gonic/gin"
	"github.com/pankaj-nupay/blog-app/controllers"
	"github.com/pankaj-nupay/blog-app/middlewares"
)

func Router() *gin.Engine {
	router := gin.Default()
	// router.GET("/books", func(c *gin.Context) {
	// 	c.JSON(http.StatusOK, gin.H{
	// 		"message": "hey there, it's working fine",
	// 	})
	// })
	api := router.Group("/api")
	{
		api.POST("/login", controllers.GenerateToken)
		api.POST("/signup", controllers.RegisterUser)
		secured := api.Group("/blog").Use(middlewares.Auth())
		{
			secured.GET("/:id", controllers.GetBlogById)
			secured.GET("/", controllers.GetAllBlogs)
			secured.POST("/", controllers.CreateBlog)
			secured.PUT("/:id", controllers.UpdateBlog)
			secured.DELETE("/:id", controllers.DeleteBlog)
		}
	}

	return router
}
