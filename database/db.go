package database

import (
	"fmt"
	"log"
	"os"

	"github.com/pankaj-nupay/blog-app/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DbInstance *gorm.DB
var dbError error

func NewConnection() error {
	dsn := fmt.Sprintf(
		"host=%s port=%s user=%s  password=%s  dbname=%s  sslmode=%s ",
		os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_USER"), os.Getenv("DB_PASS"), os.Getenv("DB_NAME"), os.Getenv("DB_SSLMODE"),
	)
	DbInstance, dbError = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if dbError != nil {
		log.Fatal(dbError)
		return dbError
	}

	err := DbInstance.AutoMigrate(&models.User{}, &models.Blog{})

	if err != nil {
		log.Fatal("could not migrate db")
		return err
	}

	return nil
}
