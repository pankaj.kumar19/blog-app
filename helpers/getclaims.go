package helpers

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func GetClaims(c *gin.Context) (*jwt.Token, error) {
	tokenString := c.GetHeader("Authorization")

	token, _, err := new(jwt.Parser).ParseUnverified(tokenString, jwt.MapClaims{})
	if err != nil {
		return nil, err
	}
	return token, nil
}
