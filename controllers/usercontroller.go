package controllers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/pankaj-nupay/blog-app/database"
	"github.com/pankaj-nupay/blog-app/models"
)

func RegisterUser(c *gin.Context) {
	var user models.User
	if err := c.BindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		// c.Abort();
		return
	}

	if err := user.HashPassword(user.Password); err != nil {
		fmt.Println("hashing pass in register user fn")
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}
	record := database.DbInstance.Create(&user)
	if record.Error != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "user created successfully", "data": gin.H{"userId": user.ID, "email": user.Email, "username": user.Username}})
}
