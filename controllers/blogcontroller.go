package controllers

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/pankaj-nupay/blog-app/database"
	"github.com/pankaj-nupay/blog-app/helpers"
	"github.com/pankaj-nupay/blog-app/models"
)

func GetAllBlogs(c *gin.Context) {
	var allBlogs []models.Blog

	token, err := helpers.GetClaims(c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		c.Abort()
		return
	}
	var user_id int
	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		// extracting id from claims
		id := fmt.Sprint(claims["id"])
		user_id, err = strconv.Atoi(id)
		if err != nil {
			log.Fatal(err)
			return
		}
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		c.Abort()
		return
	}

	records := database.DbInstance.Where("user_refer=?", user_id).Find(&allBlogs)
	if records.Error != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": records.Error.Error()})
		c.Abort()
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": allBlogs})

}

func GetBlogById(c *gin.Context) {
	var blog models.Blog
	paramId := c.Param("id")
	token, err := helpers.GetClaims(c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		c.Abort()
		return
	}
	var user_id int
	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		// extracting id from claims
		id := fmt.Sprint(claims["id"])
		user_id, err = strconv.Atoi(id)
		if err != nil {
			log.Fatal(err)
			return
		}
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		c.Abort()
		return
	}

	record := database.DbInstance.Where("user_refer = ? AND id = ?", user_id, paramId).Find(&blog)
	if record.Error != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		return
	}

	// check in case the blog with the given id does not exists
	if blog.Title == "" && blog.Description == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "blog with given id does not exist"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": blog})
}

func CreateBlog(c *gin.Context) {
	var blog models.Blog
	if err := c.BindJSON(&blog); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		c.Abort()
		return
	}

	token, err := helpers.GetClaims(c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		c.Abort()
		return
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		// extracting id from claims
		id := fmt.Sprint(claims["id"])
		blog.UserRefer, err = strconv.Atoi(id)
		if err != nil {
			log.Fatal(err)
			return
		}
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		c.Abort()
		return
	}

	record := database.DbInstance.Create(&blog)
	if record.Error != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "blog created successfully", "data": blog})
}

func UpdateBlog(c *gin.Context) {
	var blog models.Blog
	paramId := c.Param("id")
	token, err := helpers.GetClaims(c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		c.Abort()
		return
	}
	var user_id int
	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		// extracting id from claims
		id := fmt.Sprint(claims["id"])
		user_id, err = strconv.Atoi(id)
		if err != nil {
			log.Fatal(err)
			return
		}
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		c.Abort()
		return
	}

	record := database.DbInstance.Where("user_refer = ? AND id = ?", user_id, paramId).Find(&blog)
	if record.Error != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		return
	}

	c.BindJSON(&blog)
	database.DbInstance.Save(&blog)

	c.JSON(http.StatusOK, gin.H{"message": "blog updated successfully", "updatedData": blog})
}

func DeleteBlog(c *gin.Context) {
	var blog models.Blog
	paramId := c.Param("id")

	token, err := helpers.GetClaims(c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		c.Abort()
		return
	}
	var user_id int
	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		// extracting id from claims
		id := fmt.Sprint(claims["id"])
		user_id, err = strconv.Atoi(id)
		if err != nil {
			log.Fatal(err)
			return
		}
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		c.Abort()
		return
	}

	record := database.DbInstance.Where("user_refer = ? AND id = ?", user_id, paramId).Delete(&blog)
	fmt.Println(record.RowsAffected)
	if record.Error != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		return
	}
	if record.RowsAffected == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"message": "invalid blog id"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "blog deleted successfully"})
}
