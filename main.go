package main

import (
	"log"

	"github.com/joho/godotenv"
	"github.com/pankaj-nupay/blog-app/database"
	"github.com/pankaj-nupay/blog-app/routes"
)

func main() {
	// gin.SetMode(gin.ReleaseMode)

	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	err = database.NewConnection()

	if err != nil {
		log.Fatal("could not load the database")
	}

	router := routes.Router()
	router.Run(":8080")
}
